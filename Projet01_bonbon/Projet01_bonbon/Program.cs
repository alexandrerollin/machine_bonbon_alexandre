﻿using Project;
namespace Projet01_bonbon;
internal class Program
    {
        public static void Main(string[] args)
        {
            while (true) // boucle pour que le programme run toujours jusqu'à la fermeture de la console.
            {
                int selection = GetSelection();// on appel la fonction GetSelection et assigne le retour a l'initialisation de la variable int selection
                Candy userCandy = GetCandy(selection);// on initialise une variable de type candy la valeur assigné sera le retour de la fonction GetCandy avec comme paramètre la sélection faite dans la fonction précédente
                while (userCandy.Stock == 0) // on commence une boucle while pour vérifier si le bonbon est en stock et relance les deux première fonctions dans le cas contraire
                {
                    Board.Print(message: $"{userCandy.Name} VIDE!", selection: selection, price: userCandy.Price);// méssage d'affichage pour bonbon non disponible.
                    Thread.Sleep(2000);// Simple commande pour faire attendre 2 secondes avant le reload du programme.
                    selection = GetSelection();// On refait les deux premières commandes comme le choix de l'utilisateur n'était pas valide.
                    userCandy = GetCandy(selection);// On refait les deux premières commandes comme le choix de l'utilisateur n'était pas valide.
                }// LA SELECTION EST FAIT ET LES INFOS DU CANDY SONT MAINTENANT DANS LA VARIABLE userCandy // ON PASSE A LA PROCHAINE ÉTAPE.
                decimal prixTotal = userCandy.Price;// on récupère le prix du bonbon choisi et on l'assigne à la variable prixTotal.
                decimal total = 0;// initialisation de la variable total qui représente l'argent inséré.
                while (total < prixTotal)// boucle qui va run jusqu'à ce que le bonbon est été payé au complet ou que l'utilisateur annule.
                {
                    Board.Print(message: userCandy.Name, selection: selection, price: userCandy.Price, received: total);// commande d'affiche avec comme paramètre les différentes informations recues par le programme et qui va réagir a chaque entré d'utilisateur.
                    int choixArgent = GetCoin();// on appel la fonction GetCoin qui va retourner le choix entré par l'utilisateur qui va servir pour la suite.
                    switch (choixArgent)// commencement du switch // à chaque case on incremente la variable total selon le choix fait par l'utilisateur.
                    {
                        case (1):
                            total += 0.05m;
                            break;
                        case (2):
                            total += 0.10m;
                            break;
                        case (3):
                            total += 0.25m;
                            break;
                        case (4):
                            total += 1;
                            break;
                        case (5):
                            total += 2;
                            break;
                    }

                    if (choixArgent == 0)// conditonnelle si l'utilisateur décide d'annuler.
                    {
                        Board.Print(message: "ANNULER", received: total, returned: total);// message d'affichage pour indiquer que l'annulation a bien fonctionné.
                        break;// termine la loop pour finir le programme et passer directement à la ligne 57 du programme.
                    }

                    if (total >= prixTotal)// condition qui arrive une fois que l'utilisateur à payé le montant total ou plus.
                    {
                        Board.Print(message: "Prennez votre friandise...", selection: selection, price: prixTotal,// nous allons utiliser toutes les variables des dernières fontions pour terminer l'affichage.
                            received: total, returned: (total - prixTotal), result: userCandy.Name);// dernier affichage -- ici on retrouve toute les dernières informations recues par le program
                        userCandy.Stock -= userCandy.Stock;
                        Thread.Sleep(1000);// Simple commande pour faire attendre 1 secondes avant le reload du programme.
                    }
                }
                Console.WriteLine("Appuyez sur une touche pour acheter d'autre bonbon...");// Commande d'affichage pour demander à l'utilisateur de relancer le programme.
                Console.ReadLine();// on laisse l'utilisateur entré une donné pour relancer le programme mais on n'enregistre pas l'entrer.
            }
        }
        public static int GetSelection() // fonction qui sert a offrir un choix de bonbon à l'utilisateur er retourne ce choix.
        {
            int selection; // déclaration d'une variable int qui va servir à enregistrer le choix de l'utilisateur.
            do // on fait un do while pour que la loop run au moins 1 fois.
            {
                Board.Print(message: "VOTRE CHOIX ?"); // Simple message d'affichage.
                Console.Write("->"); // simple message d'affichage.
                selection = int.Parse(Console.ReadLine()); // assigne la valeur entré par l'utilisateur dans la variable selection qui sera retourné.
            } while (selection < 1 || selection > 25); // condition pour que la loop run aussi longtemps que le choix de l'utlisateur n'est pas valide.
            return selection; // retourne la selection de l'utilisateur.
        }
        public static Candy GetCandy(int selection) // fontion qui sert à aller chercher l'information du bonbon choisi par l'utlisateur qui retourne un type Candy et qui prend en paramètre le choix de l'utilisateur
        {
            Candy[] bonbon = LoadCandies(); // initialisation de la variable bonbon avec l'appel de la fonction LoadCandies.
            return bonbon[selection - 1]; // retour de l'information sur le bonbon choisi ( on fait -1 pour avoir la bonne case.
        }
        public static Candy[] LoadCandies() // fonction pour aller chercher l'information dand le fichier candies.date avec la class Candy.cs
        {
            Data dataManager = new Data(); // Ligne donné dans le documents.
            return dataManager.LoadCandies();// retour de l'information voulu.
        }
        public static int GetCoin() // fonction qui sert à afficher un choix pour la monnaie et retourner le choix au Main.
        {
            int choixArgent; // déclaration de la variable in choixArgent qui sera retourner.
            Console.WriteLine("[0] = Annuler"); // simple commande d'affichage.
            Console.WriteLine("[1] = 5c");
            Console.WriteLine("[2] = 10c");
            Console.WriteLine("[3] = 25c");
            Console.WriteLine("[4] = 1$");
            Console.WriteLine("[5] = 2$");
            choixArgent = int.Parse(Console.ReadLine()); // assignation de la valeur entré par l'utilisateur dans la variable choixArgent.
            return choixArgent; // retour de la valeur de lavariable choixArgent.
        }
    }